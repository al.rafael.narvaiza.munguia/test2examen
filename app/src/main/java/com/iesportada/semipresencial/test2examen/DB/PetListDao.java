package com.iesportada.semipresencial.test2examen.DB;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.iesportada.semipresencial.test2examen.DB.model.Pet;

import java.util.List;

@Dao
public interface PetListDao {

    @Query("SELECT * FROM Pet")
    List<Pet> getAllPets();

    @Query("UPDATE Pet SET nombre = :nombre, raza = :raza, imagen = :imagen, edad = :edad WHERE id = :id")
    void updateSelectedPet(String nombre, String raza, String imagen, int edad, int id);


    @Insert
    void insertPet(Pet pet);


    @Delete
    void deletePet(Pet pet);
}
