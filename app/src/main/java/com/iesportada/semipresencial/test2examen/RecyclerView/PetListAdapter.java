package com.iesportada.semipresencial.test2examen.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.iesportada.semipresencial.test2examen.Activities.EditPetActivity;
import com.iesportada.semipresencial.test2examen.DB.model.Pet;
import com.iesportada.semipresencial.test2examen.databinding.RecyclerviewCardBinding;

import java.util.List;

public class PetListAdapter extends RecyclerView.Adapter<PetListAdapter.MyViewHolder> {

    private Context context;
    private List<Pet> petList;
    private HandlePetClick clickListener;

    public PetListAdapter(Context context, HandlePetClick clickListener) {
        this.context = context;
        this.clickListener = clickListener;
    }

    public void setPetList(List<Pet> petList) {
        this.petList = petList;
        notifyDataSetChanged();
    }


    @Override
    public PetListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(RecyclerviewCardBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull PetListAdapter.MyViewHolder holder, int position) {
        Pet pet = petList.get(position);
        holder.binding.textViewPetName.setText(pet.getNombre());
        holder.binding.textViewPetRace.setText(pet.getRaza());
        holder.binding.textViewEdad.setText(String.valueOf(pet.getEdad()));
        Glide
                .with(context)
                .load(pet.getImagen())
                .into(holder.binding.imageViewPet);
        holder.binding.imageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.deletePet(petList.get(position));
            }
        });
        holder.binding.imageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clickListener.editPet(petList.get(position));
                Intent intent = new Intent(context, EditPetActivity.class);
                intent.putExtra("id", pet.getId());
                intent.putExtra("nombre", pet.getNombre());
                intent.putExtra("raza", pet.getRaza());
                intent.putExtra("imagen", pet.getId());
                intent.putExtra("edad", pet.getEdad());
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        if (petList == null || petList.size() == 0) {
            return 0;
        } else
            return petList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private RecyclerviewCardBinding binding;

        public MyViewHolder(@NonNull RecyclerviewCardBinding rvBinding) {
            super(rvBinding.getRoot());
            binding = rvBinding;
        }
    }

    public interface HandlePetClick {
        void editPet(Pet pet);

        void deletePet(Pet pet);
    }
}
