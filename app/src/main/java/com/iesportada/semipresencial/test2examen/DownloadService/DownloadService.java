package com.iesportada.semipresencial.test2examen.DownloadService;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;
import com.iesportada.semipresencial.test2examen.Activities.MainActivity;
import org.jetbrains.annotations.NotNull;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class DownloadService extends Service {
    private final String urlString = "https://dam.org.es/files/enlaces.txt";

    public DownloadService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        showToast("Creando el servicio");
    }

    private void showToast(String creando_el_servicio) {
        Toast.makeText(this, creando_el_servicio, Toast.LENGTH_SHORT).show();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        URL url = null;
        try{
            url = new URL(urlString);
            okhttpDownload(url);
        }catch (MalformedURLException m){
            showToast(m.getMessage());
            System.out.println(m.getMessage());

        }
        return super.onStartCommand(intent,flags, startId);
    }

    private void okhttpDownload(URL url) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                System.out.println(e.getMessage());
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (response.isSuccessful()) {
                        String myResponse = response.body().string();
                        sendResponse(myResponse);
                    }
                }
            }
        });
    }

    private void sendResponse(String myResponse) {
        Intent i = new Intent();
        i.setAction(MainActivity.ACTION_RESP);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.putExtra("resultado", myResponse);
        sendBroadcast(i);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        showToast("Servicio destruido");
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }




}
