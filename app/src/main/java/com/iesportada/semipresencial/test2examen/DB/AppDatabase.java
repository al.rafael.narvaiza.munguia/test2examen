package com.iesportada.semipresencial.test2examen.DB;


import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import com.iesportada.semipresencial.test2examen.DB.model.Pet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Pet.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {


    public abstract PetListDao petListDao();

    public static AppDatabase INSTANCE;
    private static final int THREADS = 6;

    public static final ExecutorService executorService = Executors.newFixedThreadPool(THREADS);

    public static AppDatabase getDBinstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "protectoradb")
                    .allowMainThreadQueries()
                    .build();
        }

        return INSTANCE;
    }
/*
    private static List<Pet> petList = new ArrayList<>();

    public static void addPetToList(Pet pet){
        petList.add(pet);
    }


    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDBAsync(INSTANCE).execute();
        }
    };

    private static class PopulateDBAsync extends AsyncTask<Void, Void, Void> {

        Pet pet;
        private PetListDao petListDao;

        private PopulateDBAsync(AppDatabase db) {
            petListDao = db.petListDao();
        }


        @Override
        protected Void doInBackground(Void... voids) {

            for(int i = 0;i < petList.size();i++){
                petListDao.insertPet(petList.get(i));
            }

            return null;
        }
    }

 */
}
