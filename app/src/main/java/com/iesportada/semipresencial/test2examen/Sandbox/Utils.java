package com.iesportada.semipresencial.test2examen.Sandbox;

import android.content.Context;
import android.text.TextUtils;

public class Utils {




    public static String inputFieldsAreChecked(String nombre, String raza, String imagen, int edad) {
        String mensaje = "";
        boolean error = false;
        if (TextUtils.isEmpty(nombre)) {
            mensaje = mensaje + "\nEs necesario introducir un nombre para el perro.";
            error = true;
        }
        if (TextUtils.isEmpty(raza)) {
            mensaje = mensaje + "\nEs necesario introducir una raza para el perro.";
            error = true;
        }
        if (TextUtils.isEmpty(imagen)) {
            mensaje = mensaje + "\nEs necesario seleccionar una imagen";
            error = true;
        }
        if (TextUtils.isEmpty(String.valueOf(edad))) {
            mensaje = mensaje + "\nEs necesario seleccionar una edad";
            error = true;
        }
        if (!error) {
            return "";
        }
        else{
            return mensaje;
        }
    }

}
