package com.iesportada.semipresencial.test2examen.VM;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.iesportada.semipresencial.test2examen.DB.model.Pet;
import com.iesportada.semipresencial.test2examen.repo.Repository;

import java.util.ArrayList;
import java.util.List;

public class MainActivityViewModel extends AndroidViewModel {

    private MutableLiveData<List<Pet>> listOfPets;
    private final Repository repository;
    private List<Pet>ListOfPets;

    public MainActivityViewModel(@NonNull Application application){
        super(application);
        repository = new Repository(application);
        listOfPets = new MutableLiveData<>();
    }

    public MutableLiveData<List<Pet>> getListOfPetsObserver(){
        return listOfPets;
    }


    public void getAllPetList(){
        List<Pet> petList = repository.getAllPetList();
        if(petList.size() > 0){
            listOfPets.postValue(petList);
        }
        else {
            listOfPets.postValue(null);
        }
    }

    public void setAllPetList(Pet pet){
        List<Pet> petList = new ArrayList<>();
        petList.add(pet);
    }

    public void insertBooking(String nombre, String raza, String imagen, int edad){
        Pet pet = new Pet();
        pet.setNombre(nombre);
        pet.setEdad(edad);
        pet.setRaza(raza);
        pet.setImagen(imagen);
        repository.insert(pet);
        getAllPetList();
    }

    public void updateSelectedPet(Pet pet){
        repository.update(pet);
        getAllPetList();
    }

    public void deletePet(Pet pet){
        repository.delete(pet);
        getAllPetList();
    }

}
