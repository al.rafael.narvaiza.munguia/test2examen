package com.iesportada.semipresencial.test2examen.repo;

import android.content.Context;

import com.iesportada.semipresencial.test2examen.DB.AppDatabase;
import com.iesportada.semipresencial.test2examen.DB.model.Pet;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Repository {
    private AppDatabase appDatabase;

    public Repository(Context context){
        appDatabase = AppDatabase.getDBinstance(context);
    }

    public List<Pet> getAllPetList(){
        List<Pet> list = new ArrayList<>();
        Future<List<Pet>> future = AppDatabase.executorService.submit(()->appDatabase.petListDao().getAllPets());
        try {
            list = future.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void insert(Pet pet){
        AppDatabase.executorService.execute(()->appDatabase.petListDao().insertPet(pet));
    }

    public void update(Pet pet){
        AppDatabase.executorService.execute(()->appDatabase.petListDao().updateSelectedPet(pet.getNombre(), pet.getRaza(), pet.getImagen(), pet.getEdad(), pet.getId()));

    }

    public void delete(Pet pet){
        AppDatabase.executorService.execute(()->appDatabase.petListDao().deletePet(pet));
    }
}
