package com.iesportada.semipresencial.test2examen.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.iesportada.semipresencial.test2examen.R;
import com.iesportada.semipresencial.test2examen.Sandbox.Utils;
import com.iesportada.semipresencial.test2examen.VM.MainActivityViewModel;
import com.iesportada.semipresencial.test2examen.databinding.ActivityAddPetBinding;


public class AddPetActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{


    private ActivityAddPetBinding binding;
    private MainActivityViewModel viewModelInsert;
    Utils utils = new Utils();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pet);
        binding = ActivityAddPetBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        viewModelInsert = new ViewModelProvider(this).get(MainActivityViewModel.class);

        binding.buttonDiscard.setOnClickListener(v->finish());
        binding.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre, raza, imagen;
                int edad;
                nombre = binding.editTextTextPerroName.getText().toString();
                raza = binding.editTextTextPerroRaza.getText().toString();
                imagen = binding.editTextTextPerroImagen.getText().toString();
                edad = Integer.parseInt(binding.editTextNumber.getText().toString());
                //utils.setContext(getApplicationContext());
                if(!Utils.inputFieldsAreChecked(nombre, raza, imagen, edad).isEmpty()){

                    showMessage(Utils.inputFieldsAreChecked(nombre, raza, imagen, edad));
                }
                else {
                    viewModelInsert.insertBooking(nombre, raza, imagen, edad);
                    finish();
                }
            }
        });
        viewModelInsert.getAllPetList();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        parent.getItemAtPosition(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}