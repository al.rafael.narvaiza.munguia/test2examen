package com.iesportada.semipresencial.test2examen.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.iesportada.semipresencial.test2examen.DB.model.Pet;
import com.iesportada.semipresencial.test2examen.Sandbox.Utils;
import com.iesportada.semipresencial.test2examen.VM.MainActivityViewModel;
import com.iesportada.semipresencial.test2examen.databinding.ActivityEditPetBinding;

import java.util.List;


public class EditPetActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private ActivityEditPetBinding binding;
    private MainActivityViewModel viewModelUpdate;
    private Pet updatedPet;
    Utils utils = new Utils();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEditPetBinding.inflate(getLayoutInflater());
        View v = binding.getRoot();
        setContentView(v);
        initViewModel();
        getIncomingIntent();
        binding.buttonDiscard.setOnClickListener(v1->finish());
        binding.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre,raza,imagen;
                int edad;
                updatedPet = new Pet();
                nombre = binding.editTextTextPerroName.getText().toString();
                raza = binding.editTextTextPerroRaza.getText().toString();
                imagen = binding.editTextTextPerroImagen.getText().toString();
                edad = Integer.parseInt(binding.editTextNumber.getText().toString());
                updatedPet.setNombre(nombre);
                updatedPet.setRaza(raza);
                updatedPet.setImagen(imagen);
                updatedPet.setEdad(edad);
                updatedPet.setId(Integer.valueOf(binding.textViewId.getText().toString()));
                //utils.setContext(getApplicationContext());
                if(!Utils.inputFieldsAreChecked(nombre, raza, imagen, edad).isEmpty()){
                    showMessage(Utils.inputFieldsAreChecked(nombre, raza, imagen, edad));
                }
                else{
                    viewModelUpdate.updateSelectedPet(updatedPet);
                    finish();
                }
            }
        });
    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
    private void initViewModel() {
        viewModelUpdate = new ViewModelProvider(this).get(MainActivityViewModel.class);
        viewModelUpdate.getListOfPetsObserver().observe(this, new Observer<List<Pet>>() {
            @Override
            public void onChanged(List<Pet> pets) {

            }
        });
    }

    private void getIncomingIntent(){
        String nombre, raza, imagen;
        int edad, id;
        if(getIntent().hasExtra("id")
                && getIntent().hasExtra("nombre")
                &&getIntent().hasExtra("raza")
                &&getIntent().hasExtra("edad")
        ){
            id = getIntent().getIntExtra("id", 0);
            edad = getIntent().getIntExtra("edad", 0);
            nombre = getIntent().getStringExtra("nombre");
            raza = getIntent().getStringExtra("raza");
            imagen = getIntent().getStringExtra("imagen");

            binding.textViewId.setText(String.valueOf(id));
            binding.editTextTextPerroImagen.setText(imagen);
            binding.editTextTextPerroName.setText(nombre);
            binding.editTextTextPerroRaza.setText(raza);
            binding.editTextNumber.setText(String.valueOf(edad));
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        parent.getItemAtPosition(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}