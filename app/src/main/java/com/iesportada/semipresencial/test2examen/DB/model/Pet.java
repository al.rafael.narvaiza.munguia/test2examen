package com.iesportada.semipresencial.test2examen.DB.model;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Pet {
    @NonNull
    @ColumnInfo(name="id")
    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    @ColumnInfo(name = "nombre")
    private String nombre;

    @NonNull
    @ColumnInfo(name = "raza")
    private String raza;

    @NonNull
    @ColumnInfo(name = "imagen")
    private String imagen;

    @NonNull
    @ColumnInfo(name = "edad")
    private int edad;

    public Pet(@NonNull String name, String race, String image, int age){
        this.setId(getId());
        this.setNombre(name);
        this.setRaza(race);
        this.setEdad(age);
        this.setImagen(image);
    }

    public Pet() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getNombre() {
        return nombre;
    }

    public void setNombre(@NonNull String nombre) {
        this.nombre = nombre;
    }

    @NonNull
    public String getRaza() {
        return raza;
    }

    public void setRaza(@NonNull String raza) {
        this.raza = raza;
    }

    @NonNull
    public String getImagen() {
        return imagen;
    }

    public void setImagen(@NonNull String imagen) {
        this.imagen = imagen;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
}

