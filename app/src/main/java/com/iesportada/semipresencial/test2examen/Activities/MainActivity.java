package com.iesportada.semipresencial.test2examen.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.iesportada.semipresencial.test2examen.DB.AppDatabase;
import com.iesportada.semipresencial.test2examen.DB.model.Pet;
import com.iesportada.semipresencial.test2examen.DownloadService.DownloadService;
import com.iesportada.semipresencial.test2examen.R;
import com.iesportada.semipresencial.test2examen.RecyclerView.PetListAdapter;
import com.iesportada.semipresencial.test2examen.VM.MainActivityViewModel;
import com.iesportada.semipresencial.test2examen.databinding.ActivityMainBinding;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class MainActivity extends AppCompatActivity implements PetListAdapter.HandlePetClick{

    public static final String ACTION_RESP = "RESPUESTA_DESCARGA";
    private MainActivityViewModel viewModel;
    ActivityMainBinding binding;
    PetListAdapter petListAdapter;
    Intent intent;
    IntentFilter intentFilter;
    BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        intentFilter = new IntentFilter(ACTION_RESP);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastReceiver = new Receiver();

        intent = new Intent(MainActivity.this, DownloadService.class);
        startService(intent);

        binding.addNewwBookingImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddPetActivity.class);
                startActivity(intent);
            }
        });

        initViewModel();
        initRecyclerView();
        viewModel.getAllPetList();
    }

    private void initRecyclerView() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        petListAdapter = new PetListAdapter(this, this);
        binding.recyclerView.setAdapter(petListAdapter);
    }

    @Override
    public void onResume(){
        super.onResume();
        registerReceiver(broadcastReceiver, intentFilter);
        viewModel.getAllPetList();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        showMessage("Servicio parado");
        stopService(intent);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        registerReceiver(broadcastReceiver, intentFilter);
        binding.recyclerView.setVisibility(View.VISIBLE);
        viewModel.getAllPetList();
    }

    private void initViewModel() {
        viewModel = new ViewModelProvider(this).get(MainActivityViewModel.class);
        viewModel.getListOfPetsObserver().observe(this, new Observer<List<Pet>>() {
            @Override
            public void onChanged(List<Pet> pets) {
                if (pets == null){
                    binding.emptyListTextView.setVisibility(View.VISIBLE);
                    binding.recyclerView.setVisibility(View.GONE);
                }else{
                    petListAdapter.setPetList(pets);
                    binding.recyclerView.setVisibility(View.VISIBLE);
                    binding.emptyListTextView.setVisibility(View.GONE);
                }
            }
        });
    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String respuesta = intent.getStringExtra("resultado");
            showMessage(respuesta);
            String[] line = respuesta.split("\n");
            for (int i = 0; i < line.length; i++) {
                createPetObjectFromUrl(line[i]);
            }
            insertListOfPetsOnDB();
        }
    }

    private void insertListOfPetsOnDB() {

    }

    private void createPetObjectFromUrl(String s) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(s).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (response.isSuccessful()) {
                        String myResponse = response.body().string();
                        insertInDB(myResponse);
                    }
                }
            }
        });
    }


    private void insertInDB(String respuesta) {
        Pet pet = new Pet();
        String[] pets = respuesta.split("[:\n]");

        pet.setNombre(pets[1].trim());
        pet.setRaza(pets[3].trim());
        pet.setEdad(Integer.parseInt(pets[5].trim()));
        pet.setImagen("https://dam.org.es/" + pets[7].trim());
        viewModel.insertBooking(pet.getNombre(), pet.getRaza(), pet.getImagen(), pet.getEdad());
        /*
        int i = 0;
        try{
            while(viewModel.getListOfPetsObserver().getValue().size()<=5){
                try{
                    i = viewModel.getListOfPetsObserver().getValue().size();
                }catch (NullPointerException e){
                    System.out.println(e.getMessage());
                }

            }
        }catch (NullPointerException e){
            System.out.println(e.getMessage());
        }
        if(i<=5){
            viewModel.insertBooking(pet.getNombre(), pet.getRaza(), pet.getImagen(), pet.getEdad());
        }

         */

        viewModel.getAllPetList();
    }



    @Override
    public void editPet(Pet pet) {

    }

    @Override
    public void deletePet(Pet pet) {
        viewModel.deletePet(pet);
    }
}